FROM alpine:3.5 AS base
RUN apk add --no-cache nodejs-current
WORKDIR /shipping_tracking
COPY package.json .

# ---- Dependencies ----
FROM base AS dependencies
RUN npm set progress=false && npm config set depth 0
RUN npm install

# ---- Test ----
FROM dependencies AS test
COPY . .
RUN npm test

# ---- Release ----
FROM test AS release
COPY . .
CMD node bin/www
EXPOSE 3000
