##Shipping Tracking

**Description**

* simple project about tracking delivery process

**Content and technologies**

* BackEnd Service : node js - Express
* FrontEnd : angular.js - bootstrap - font-awesome
* Unit Test : chai - chai-http - chai expect
* Docker and Docker compose for both front end and back end service
* gitlab CI-CD

**Installation**

* https://gitlab.com/rashaAbuelmagd/shipping_tracking.git
* npm install

if you want to run the project

* nodemon modeule is intalled you can run with it by :-   **nodemon bin/ww**

* if you want to run Unit test :- **npm test**

**To gitlab CI/CD
* you need to install and register gitlab runner: install it from https://docs.gitlab.com/runner/install/
