const models = require('../models/');
const request = require('request');
exports.createTask = function(req, res) {
  request('https://api.myjson.com/bins/b9ix6', function(error, response, body) {
    if (!error && response.statusCode == 200) {
      var tasksData = JSON.parse(body);
      models.tasks.bulkCreate(tasksData['tasks'])
        .then(function(data) {
          res.status(200).json({
            message: 'Created Successfully',
            data: data
          });
        }).catch(function(err) {
          res.status(500).json({
            message: err.message,
            data: []
          });
        });
    }
  });
};
exports.getTasks = function(req, res) {
  models.tasks.findAll({
      order: [
        [req.body.orderByCol, req.body.sortingType]
      ]
    })
    .then(function(tasks) {
      return res.status(200).json({
        message: 'Data Retrieved Successfully',
        data: tasks
      });
    })
    .catch(function(err) {
      res.status(500).json({
        message: err.message,
        data: []
      });
    });
}
exports.updateTasks = function(req, res) {
  console.log(req.body);
  if (req.body.OldStatus == 'pending') {
    models.tasks.update({
        status: req.body.status
      }, {
        where: {
          ID: req.params.taskID
        }
      })
      .then(function(tasks) {
        res.status(200).json({
          message: 'Updated Successfully',
          data: tasks
        });
      })
      .catch(function(err) {
        res.status(500).json({
          message: err.message,
          data: []
        });
      });
  } else {
    res.status(400).json({
      message: 'You Cannot Update Your Task as it is not in pending state',
      data: []
    });
  }
}
