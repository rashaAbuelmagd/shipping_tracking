/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  var tasks = sequelize.define('tasks', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      autoIncrement: true,
      primaryKey: true
    },
    fromLocation: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    toLocation: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    deliveryDate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    startedAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    finishedAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    driverName: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    courier: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    driverComment: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    status: {
      type: DataTypes.STRING(45),
      allowNull: true
    }
  }, {
    tableName: 'tasks',
    timestamps: false
  });
  return tasks;
};
