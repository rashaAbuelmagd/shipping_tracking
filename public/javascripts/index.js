angular.module('angularTable', [])
  .controller('Tasks', function($scope, $http) {
    $scope.tasks = [];
    $scope.getTasks = function(orderByCol, sortingType) {
      $http({
        method: 'POST',
        url: 'http://localhost:3000/Tasks/read',
        data: {
          'orderByCol': orderByCol,
          'sortingType': sortingType
        }
      }).then(function(tasks) {
        $scope.tasks = tasks.data.data;
      });
    };
    $scope.updateTasks = function(taskID, status, OldStatus) {
      $http({
        method: 'POST',
        url: 'http://localhost:3000/Tasks/update/' + taskID,
        data: {
          'OldStatus': OldStatus,
          'status': status
        }
      }).then(function() {
        $scope.list = $scope.tasks.filter(function(element) {
          return element.ID == taskID;
        });
        $scope.list[0].status = status;
      });
    };
    $scope.sort = function(keyname) {
      $scope.sortKey = keyname;
      $scope.reverse = !$scope.reverse;
    }
  });
