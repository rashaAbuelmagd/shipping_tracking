const express = require('express');
const router = express.Router();

const TasksController = require('../controllers/tasks');

router.post('/create', TasksController.createTask);
router.post('/update/:taskID', TasksController.updateTasks);
router.post('/read', TasksController.getTasks);
router.get('/view', function(req, res) {
  res.sendfile('public/index.html');
});
module.exports = router;
