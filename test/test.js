'use strict';
process.env.NODE_ENV = 'test';
const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../app');
const should = chai.should();
const expect = chai.expect;
const tasksController = require('../controllers/tasks');
chai.use(chaiHttp);

describe('Tasks Controller Test', function() {
  it('Shuold read data from url and insert them into DB , returning created data', function() {
    return chai.request(app)
      .post('/Tasks/create')
      .send()
      .then(function(res) {
        expect(res).to.have.status(200);
        expect(res.body).to.have.property('message').eql('Created Successfully');
        expect(res.body).to.have.property('data');
        expect(res).to.be.json;
      }).catch(function(err) {
        expect(err).to.have.status(500);
        expect(res.body).to.have.property('message');
      });
  });

  it('Shuold retrive all tasks', function() {
    return chai.request(app)
      .post('/Tasks/read')
      .send({
        'orderByCol': 'status',
        'sortingType': 'ASC',
      })
      .then(function(res) {
        expect(res).to.have.status(200);
        expect(res.body).to.have.property('message').eql('Data Retrieved Successfully');
        expect(res.body).to.have.property('data');
        expect(res).to.be.json;
      }).catch(function(err) {
        expect(err).to.have.status(500);
        expect(res.body).to.have.property('message');
      });
  });

  it('Shuold update pending status into faild or complated', function() {
    return chai.request(app)
      .post('/Tasks/update/10')
      .send({
        'OldStatus': 'pending',
        'status': 'complated'
      })
      .then(function(res) {
        expect(res).to.have.status(200);
        expect(res.body).to.have.property('message').eql('Updated Successfully');
        expect(res.body).to.have.property('data');
      }).catch(function(err) {
        expect(err).to.have.status(500);
        expect(res.body).to.have.property('message');
      });
  });
})
